package ejercicio1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DateTree {

	public static void main(String[] args) throws IOException {
		Path carpetaGrande=Paths.get("C:\\Users\\monts\\Desktop\\a");//ruta
		
		List<Path> archivos =  Files.walk(carpetaGrande).filter(Files::isRegularFile).collect(Collectors.toList());
		
		for(Path a:archivos) {//mueve a la carpeta grande
			Files.move(a, carpetaGrande.resolve(carpetaGrande+"\\"+a.getFileName()));
		}
		
		Files.walk(carpetaGrande)
		.sorted(Comparator.reverseOrder())
		.filter(Files::isDirectory)
		.forEach(carpeta -> {//borra todas las carpetas
			try {
				if (!carpeta.equals(carpetaGrande)) {
					Files.delete(carpeta);
				}
			} catch (IOException e) {e.printStackTrace();}
		});
		
		Files.walk(carpetaGrande)//crea nuevas
		//no se puede con el list porque usa la ruta antigua o algo asi
		.filter(Files::isRegularFile)
		.forEach( file -> {
			LocalDateTime time;
			try {
				time = LocalDateTime.parse(Files.getLastModifiedTime(file).toString(), DateTimeFormatter.ISO_DATE_TIME);
				Path carpetaAnio = carpetaGrande.resolve(carpetaGrande+"\\" + time.getYear() + "\\" + time.getMonthValue() + "\\" + time.getDayOfMonth());
				Files.createDirectories(carpetaAnio);
				//con createsDirectories no crea de mas si ya existe
				
				Files.move(file, carpetaAnio.resolve(file.getFileName()));//lo mueve
			} catch (IOException e) {e.printStackTrace();}
			
	});
		
		//----------------
		/*try {
			Files.walk(carpetaGrande)//mira en toda la carpeta
			.filter(Files::isRegularFile)//esto saca solo los archivos
			.forEach(file-> {//a cada cosa que encuentra la llama file
							LocalDateTime time;
							try {
								time = LocalDateTime.parse(Files.getLastModifiedTime(file).toString(), DateTimeFormatter.ISO_DATE_TIME);
								//mirar el anio. Crear una carpeta con ese anio. Pero si existe no crearla
								//hay que volver a ver el contenido de la carpeta
								//System.out.println(time);
																
								//------------------------------
							/*	System.out.println("ARCHIVO: "+file.getFileName());
								System.out.println("Anio del archivo : "+time.getYear());//anio					
								System.out.println("Mes del archivo: "+time.getMonth());//mes en letras
								System.out.println("Mes del archivo: (2) "+time.getMonthValue());//mes en numberos
								System.out.println("Dia del mes del archivo: "+time.getDayOfMonth());//dia del mes*/
								

								//get
							//} catch (IOException e) {e.printStackTrace();}

							
						//}
					//);
			
		//} catch (IOException e) {e.printStackTrace();}

	}

}
