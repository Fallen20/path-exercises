/*
 * Montse Gordillo Cumplido 2WIAM
 * */
package ejercicio1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;

public class Flatten {
	public static void main(String[] args) {
		Path carpetaGrande=Paths.get("/home/users/inf/wiam2/iam47264842/Escritorio/niats");
		try {
			Files.walk(carpetaGrande)//mira en toda la carpeta
			.filter(Files::isRegularFile)//esto saca solo los archivos
			.forEach(file-> {//a cada cosa que encuentra la llama file
							try {
								Files.move(file, carpetaGrande.resolve(file.getFileName()));
								//move: el primero en este caso tiene la ruta antigua del archivo
								//el segundo le pones la ruta nueva, pero ha de tener el nombre con el que quieres que se guarde en la nueva ruta
							} catch (IOException e) {e.printStackTrace();}
						}
					);
			
				//.forEach(System.out::println);//por cada uno que me encuentres saca una linea
		} catch (IOException e) {e.printStackTrace();}
		
		System.out.println("archivos sacados");
		
		
		try {
			Files.walk(carpetaGrande)
			.sorted(Comparator.reverseOrder())//lo ordena al reves
			.filter(Files::isDirectory)//busca las carpetas
			.forEach(carpeta ->{
				if(!carpeta.getFileName().equals(carpetaGrande.getFileName())) {
					try {
						Files.delete(carpeta);
					} catch (IOException e) {e.printStackTrace();}
					//System.out.println(carpeta.getFileName());
				}
			});
		} catch (IOException e) {e.printStackTrace();}
		
		
	}
}
