package ejercicio1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Treediff {

	static boolean encontrado=false;//esto es porque trabajamos en lamba y no puede acceder a variables normales
	
	public static void main(String[] args) throws IOException, NoSuchAlgorithmException{
		Path carpeta1= Paths.get("C:\\Users\\monts\\Desktop\\ej2\\dirA");//lee la ruta
		Path carpeta2= Paths.get("C:\\Users\\monts\\Desktop\\ej2\\dirB");
		
		List<Path> dirA =  Files.walk(carpeta1).filter(Files::isRegularFile).collect(Collectors.toList());
        List<Path> dirB =  Files.walk(carpeta2).filter(Files::isRegularFile).collect(Collectors.toList());

		try {
			Files.walk(carpeta1)//mira en toda la carpeta
			.filter(Files::isRegularFile)
			//o se puede poner collectAllInArray y hacer for normales
			.forEach(file-> {//por cada cosa que encuentres
				//System.out.println(file);
						try {
							byte[] archivoA = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file));//lee los archivos que encuentra y lo guarda en una array
							//esto lee uno y lo sobrescribe a la siguiente ronda
							encontrado=false;
							try {
								Files.walk(carpeta2)//mira en toda la carpeta
								.filter(Files::isRegularFile)
								.forEach(fileB-> {//por cada cosa que encuentres
									//System.out.println(file);
											try {
												byte[] archivoB = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(fileB));//hash de los archivosB
												
												if (Arrays.equals(archivoA, archivoB)) {//mira si tienen el mismo CONTENIDO
													System.out.println("El contenido de "+file.getFileName() 
														+" en DirA es igual al contenido de "+fileB.getFileName()+" en DirB");
													//los archivos de A estan en B
													encontrado=true;
												}
												
												
												
											}
											catch (NoSuchAlgorithmException e) {e.printStackTrace();}
											catch (IOException e) {e.printStackTrace();}

									}
								);
								if(encontrado==false) {System.out.println("ERROR - "+file.getFileName()+" con el mismo contenido NO se ha encontrado en DirB");}
								//los archivos de A no estan en B
								
							} catch (IOException e) {e.printStackTrace();}
							
						}
						catch (NoSuchAlgorithmException e) {e.printStackTrace();}
						catch (IOException e) {e.printStackTrace();}

				}
			);
		} catch (IOException e) {e.printStackTrace();}
		System.out.println();
		encontrado=false;
		
		//EJ3  y 4 MANERA2
		for(Path file:dirB) {
            byte[] archivoB2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file));
            encontrado=false;
            for(Path file2:dirA) {
                byte[] archivoA2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file2));
                if (Arrays.equals(archivoB2, archivoA2)) {encontrado=true;}
            }
            if(encontrado==false) {System.out.println("ERROR - El archivo "+file.getFileName()+" NO se ha encontrado en DirA");}
            else {System.out.println("El archivo "+file.getFileName()+" se ha encontrado en DirA");}
            
        }
		
		System.out.println();
		//EJ5
		for(Path file:dirB) {
            byte[] archivoB2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file));
            encontrado=false;
            for(Path file2:dirA) {
                byte[] archivoA2 = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(file2));
                if (Arrays.equals(archivoB2, archivoA2)) {
                	if(carpeta2.relativize(file).equals(carpeta1.relativize(file2))) {
                		System.out.println(file.getFileName()+" tiene la misma ruta que "+file2.getFileName());
                		System.out.println("\tRuta file: "+file);
                		System.out.println("\tRuta file2: "+file2);
                	}
                }
            }
            
            
        }

		

	}

}


/*
 dirA   //pc
  * 
  *pc///a/b/f1.jpg
  * 
  *dirB  /pen
  *
  *///pen/a/b/f1r.jpg //NO MISMA RUTA
 
  //relativitze
 

